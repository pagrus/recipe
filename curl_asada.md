from 
https://sweetsimplevegan.com/2019/07/vegan-carne-asada-tacos/

Description

Get ready to enjoy some classic carne asada tacos…made without the meat! This vegan version of the classic Mexican recipe is spot on to what I remember it to taste like growing up. It’s full of flavor, easy to make and packed with plant-protein!
Ingredients

    1 bag (8 oz.) Butler’s Soy Curls
    4 cups vegan “beef broth” (we used this one)
    2 cup fresh cilantro, chopped
    1 cup freshly squeezed orange juice + rinds
    1 red onion, thinly sliced into rings
    8 garlic cloves, minced
    Juice of 4 limes
    5 tablespoons tamari (or soy sauce if not gluten-free)
    2 tablespoons apple cider vinegar (or white vinegar)
    1 teaspoon cumin
    1 teaspoon chili powder
    ⅓ teaspoon black pepper
    Optional: 2 jalapeños, seeded and diced
    1 tablespoon olive or avocado oil

Serve with:

    10 corn tortillas
    1 cup white onion, diced
    ½ cup cilantro, diced
    10 Lime wedges (about 2 limes)

Instructions

- Heat the vegan beef broth in a pot over medium heat and bring it to a boil. Once it is boiling, remove it from heat.
- Place the soy curls into a large bowl or dish and pour the vegan beef broth over the soy curls. 
- Using a spatula, mix the soy curls and and press them down into the broth if needed to make sure that they get hydrated.
- Allow the soy curls to sit for 10 minutes. 
- We transferred the soy curls to a large resealable bag after hydrating them, but it is not necessary, you can just keep it in a bowl or dish if needed. 
- Add the remaining ingredients except for the oil and mix until well combined.
- Seal the bag or cover the bowl/dish with a lid and place it into the fridge to marinate overnight. 
- If you are in a hurry, you can remove it after 2 hours but overnight yields the best result!

Grab a large cast iron skillet (or a large skillet of your choice) and set it over medium heat with the remaining 1 tablespoon of oil. 
Using tongs, slowly remove the curls from the bag and into the pan, trying your best not to transfer the onions into the pan and to leave them in the bag. 
Add 1/2 cup of the marinade juice from the bag into the pan at a time and add more once the liquid cooks off. 
Continue until you have used up all of the marinade and cook until the carne asada has browned. 
If you would like, you can transfer the carne asada to a grill pan and finish it up there with nice grill marks. It is not necessary but it does make them look nice!
    Set the carne asada aside and allow it to cool. Once it has cooled, transfer it to a cutting board and chop it into bite sized pieces.
    Prepare your tortillas. Grab a clean kitchen towel and place it next to your stovetop. Turn the burner on low and using tongs, place one tortilla on the burner. Heat the tortilla for about 45 seconds, or until slightly charred, and flip. Cook the second side until it has charred to your liking. Keep in mind that the second side will take less time than the first.  When the tortilla is finished, transfer it to the clean towel and cover it with the towel. Continue until you have finished all of the tortillas, transferring them into the towel and covering the tortillas each time you add more. Cover the tortillas will keep them warm and soft.
    Assemble the tacos as desired. We added cilantro, white onion and fresh lime. Enjoy!
