# Breakfast Burritos

- one package of faux chorizo eg tofurky chorizo, seitan chorizo, soyrizo, tj mock chorizo, etc
- 1 tablespoon safflower oil
- two packages soft tofu
- 1/4t salt
- flour taco-sized tortillas
- can of black beans
- follow your heart pepper jack slices
- sliced jalapeno
- salsa
- aluminum foil sheets 

Cook the fauxrizo in oil, just until browned. Combine with tofu in a bowl, mix until combined and as lumpy as you like it. Add salt. 

If you are refrying your beans do it in the same pan-- add oil, can of beans, some salt, some water. Cook and smush until desired consistency

Put down a sheet of foil, lay a tortilla on top. Center a square of faux cheese.

Add one scoop black beans, one scoop tofu scramble.

Add sliced jalapeno, salsa

Using the sides of the tortilla, spread the fillings out sideways. Fold sides in, then roll from the bottom away from you.

When the top edge of the tortilla is rolled in, follow with foil in the same direction

Crumple and twist the ends of the foil, push in until the surface is taut

Roll up in second foil sheet

Freeze! Or refrigerate. Or heat, I guess, if you want
