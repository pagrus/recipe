# choco chip cookie

pretty much almost just the king arthur vegan cookie recipe, with a couple very minor tweaks

## Ingredients

### dries
- 2 cups (240g) King Arthur Unbleached All-Purpose Flour
- 1 teaspoon baking powder
- 3/4 teaspoon baking soda
- 1/2 teaspoon table salt
- 1 1/4 cups (213g) vegan chocolate chips

### wets
- 1 cup (198g) granulated sugar
- 1 teaspoon (7g) molasses
- 1 teaspoon vanilla (4g)
- 1/2 cup plus 1 tablespoon (106g) vegetable oil
- 1/4 cup plus 1 tablespoon (71g) water
- flaky sea salt, for garnish

Instructions

To make the dough: In a large bowl, whisk together the flour, baking powder, baking soda, and salt. Add the chocolate chips and toss to coat.

In a separate large bowl, whisk the sugars with the oil and water until smooth, about 2 minutes. Add the flour mixture and stir just until everything is combined and no flour is visible. Don't overmix.

 Cover with plastic wrap and refrigerate the dough for at least 12 hours and up to 24 hours. Don't skip this step!

 To bake: Preheat the oven to 350°F. Line two baking pans with parchment.

 Remove the dough from the refrigerator and scoop it by the tablespoonful, dropping mounds of dough onto the baking sheet and leaving 2" of space between each mound. Place the unbaked cookies in the freezer for 10 minutes; this will help the cookies retain their shape.

 Just before the cookies go into the oven, sprinkle them with flaky sea salt. Bake for 12 to 13 minutes, or until the edges are just golden. Be careful not to overbake.

 Remove the cookies from the oven and cool completely on the pan before serving.

 Store any leftovers, well wrapped, at room temperature for several days; freeze for longer storage.

recipe:
https://www.kingarthurbaking.com/recipes/vegan-salted-chocolate-chip-cookies-recipe
