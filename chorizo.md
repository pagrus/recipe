# chorizo, mexican style

## inspiration slash reference
- https://honestcooking.com/authentic-homemade-mexican-chorizo/

Authentic Homemade Mexican Chorizo
 
Print
Prep Time
30 mins
Total Time
30 mins
 
How to make authentic Mexican chorizo at home.
Author: Nancy Lopez McHugh
Recipe Type: Main
Cuisine: Mexican
Ingredients

    1.10 lb. or 500 grams of ground pork

    1 tbsp. cumin seed = 7.01g
    1 tsp. coriander seed = 1.90g
    5 whole cloves = 0.49g
    2 bay leaves = 0.37g
    ¼ tsp. ground cinnamon = 0.77g
    ½ tsp. oregano = 0.86g
    ½ tsp. thyme = 0.65g
    1 tbsp. granulated garlic = 11.71g
    1 tsp. sea salt = 3.60g (kosher)
    5 whole peppercorns (or ½ tsp. ground black pepper) = 0.22g
    2 tbsp. Ancho chile powder OR substitute with a combination of = 14.40g + 2.92
    2 tbsp. paprika mixed with ½ tsp. cayenne powder OR
    2 tbsp. paprika mixed with 1 tsp. red chili powder

makes 44.11g total
25g for 10oz of meat (0.566 of a recipe)

    3 tbsp. apple cider vinegar (can substitute with
    red wine vinegar)
51.09g total
28.92g for 10oz

Instructions

    Handling chili powders can cause burning to your hands. I
    highly advice to use plastic gloves for handling the chili and
    chorizo.
    In a mortar and pestle grind the cumin seed, coriander seed and cloves. Break up the bay leaves with your hands as much as possible and add them to the spices in mortar and pestle, grind until you have a fine powder. Next add the remaining spices to the mortar and pestle and grind/mix until everything is well combined.
    In a large glass bowl using your hands break up the the ground pork. Next you may want to put on some plastic gloves to prevent burning and staining to your hands. Pour in the vinegar and half the spices, spread them evenly on the pork, and start working it into the ground pork. Adding more of the spice mixture until it has all been used up. Keep working the meat until it turns red (from the chile) and all the spices have been well combined into the meat.
    It is best to let the chorizo sit overnight before cooking with it. This will allow all of the flavors to come together and make for a better tasting chorizo.
    Alternatively you can freeze the chorizo until you are ready to use it. Since this chorizo does not have casings you can form or shape sausages or patties and keep their form by wrapping them in plastic kitchen wrap. Another way to store the chorizo in the freezer is by portioning out amounts and storing it inside plastic bags. This way you defrost what you need or want without having to use the whole large batch.
    Now you are ready to use the chorizo in all of your favorite Mexican recipes like tacos, sopes, frijoles charros, chiles rellenos or any of your other favorite fusion recipes.


Another one: https://www.allrecipes.com/recipe/214089/mexican-chorizo/

    2 pounds boneless pork butt (shoulder), cut into 3/4-inch pieces
    1 ½ tablespoons crushed Aleppo peppers
    1 ½ tablespoons chili powder
    4 cloves garlic, minced
    2 teaspoons salt
    1 teaspoon freshly ground black pepper
    ½ teaspoon dried oregano
    ½ teaspoon ground cumin
    ¼ teaspoon ground cloves
    ¼ teaspoon ground coriander
    ½ cup distilled white vinegar
    2 tablespoons water
    1 teaspoon vegetable oil

Directions
Instructions Checklist

    Step 1

    Place the pork, Aleppo pepper, chili powder, garlic, salt, black pepper, oregano, cumin, cloves, and coriander into a bowl, and lightly toss the pork with the seasonings until thoroughly blended. Cover the bowl, and refrigerate the meat, your meat grinder's head assembly, and grinder hopper for 1 hour.
    Step 2

    Fill a large mixing bowl with ice cubes, and place a smaller metal bowl in the ice cubes to catch the ground meat. Assemble the chilled meat grinder, and grind the pork and seasonings using a coarse cutting plate. Return ground meat to refrigerator for 30 minutes. Lightly stir the ground pork with the vinegar and water until thoroughly mixed, form into patties, and refrigerate overnight, covered, to let flavors develop.
    Step 3

    Heat vegetable oil in a heavy skillet over medium-low heat, and pan-fry the patties until browned and no longer pink in the middle, 5 to 8 minutes per side.


