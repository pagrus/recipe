# soy curl bulgogi

MAIN

    800 g / 1.76 pounds rib eye or top sirloin (or any tender prime beef cut), thinly sliced, 2mm to 3mm (1/8 inch) thickness
    1 onion (130 g / 4.6 ounces), optional, peeled & thinly sliced
    2 stalks green onion (55 g / 2 ounces), optional, thinly sliced
    1/2 carrot (55 g / 2 ounces), optional, peeled & thinly sliced
    1 Tbsp toasted sesame oil
    1 Tbsp toasted sesame seeds
    1 Tbsp cooking oil (I used rice bran oil)

BULGOGI MARINADE

    6 Tbsp soy sauce (I use regular Kikkoman soy sauce)
    3 Tbsp brown sugar
    2 Tbsp rice wine (mirin)
    1 asian pear or red apple (155 g / 5.5 ounces)
    1/2 onion (80 g / 2.8 ounces)
    1 Tbsp minced garlic
    1 tsp minced ginger
    1/8 tsp ground black pepper

alternately:

    ▢ 6 tablespoons soy sauce
    ▢ 3 tablespoons water
    ▢ 4 tablespoons sugar ( or you can use 2 T sugar 2 T honey) Use more if not using Korean pear or apple
    ▢ 2 tablespoons rice wine or mirin
    ▢ 2 tablespoons minced garlic
    ▢ 2 tablespoons sesame oil
    ▢ 2 teaspoons sesame seeds
    ▢ 4 tablespoons grated Korean/Asian pear
    ▢ ⅛ teaspoon pepper

combined/average

- 6T soy sauce
- 2T mirin
- 3T sugar/brown sugar
- 1ea pear peeled and cored
- 1ea onion rough chop
- 2T garlic
- 1t ginger grated
- 1/8t pepper (white pepper?)
- 2T sesame oil
- 2t sesame seed
- mushroom powder
- veg oil
- marmite?

blend
marinade
grill

from
https://www.koreanbapsang.com/bulgogi-korean-bbq-beef/
https://mykoreankitchen.com/bulgogi-korean-bbq-beef/
