# The laziest pizza dough ever

Makes one pizza's worth of dough

- 250g flour
- 5g salt
- 3g yeast
- 16g olive oil
- 173.5ml water
