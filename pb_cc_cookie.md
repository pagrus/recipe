# Chocolate chip peanut butter cookies

- 200g ap flour
- 3/4 t baking powder
- 1/2 t baking soda
- 1/4 t salt
- 115g coconut oil
- 225g peanut butter, chunky
- 300g white sugar
- 20g molasses
- 1t vanilla extract
- 60g garbanzo bean juice
- 150g chocolate chips

1. sift dries together
2. cream coconut oil and sugars
3. beat while pouring bean fluid in a thin stream
4. mix in peanut butter, vanilla
5. add dries
6. add choco chips
7. scoop into balls, press with a fork if you want
8. bake @ 350 about 12 minutes

Makes about 32 cookies? I think? I'm adding this later and I
can't remember now. I used a DP-40 sized disher (purple handle),
which I guess is 7/8oz. They could be a little smaller actually,
I'll get weights next time

Anyway, this is my attempt at a vegan chocolate chip peanut
butter cookie which would be the only cookie I would ever eat if
I had to pick just one. The coconut oil makes them a little weird
but I wanted something I could just use off the shelf without too
much hassle. The quantities will probably change somewhat as I
work on this, in fact I might have changed a couple things last
time I made them, I can't find my notes right now.
