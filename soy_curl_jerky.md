# soy curl jerky

## ingredients
- 2c dry soy curls - 100g
- 2c boiling water
- 1T vegetable oil - 12g
- 2T tamari - 35g
- 2T nutritional yeast - 10g
- 1T maple syrup - 16g
- 2t liquid smoke - 10g
- 1/2t onion powder - 3g
- 3/4t smoked paprika - 2g
- crushed red pepper flakes 4g 
- mushroom powder 8g
- salt 3g
- acv 25g

## instructions
- soak soy curls in water
- drain thoroughly
- mix all ingredients
- bake 30m @ 300F, stir
- bake 15-30m
- cool on sheet

ref: https://theveganharvest.com/2019/02/01/vegan-soy-curl-jerky/
