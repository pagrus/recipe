# lazy peanut noodles

- 165g fresh noodles eg shangai, etc
- peanut butter
- rice wine vinegar
- soy sauce
- garlic granules
- chile oil
- sesame oil
- crushed red pepper

cook noodles, wash in cold water, set aside

mix remaining ingredients and some noodle water, cook until thick

add noodles back in and mix

