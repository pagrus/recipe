From https://www.vietworldkitchen.com/blog/2016/12/mapo-tofu-recipe-sichuan.html

Mapo Tofu
For the classic version, use beef. It pairs beautifully with the other robust ingredients.
Servings: 4
Ingredients

    14 to 16 ounces medium or medium-firm tofu = 400-450g
    1 generous teaspoon Sichuan peppercorn = 1.75g - 
    3 tablespoons canola oil = 43g
    6 ounces ground beef or pork, fattier kind preferred, roughly chopped to loosen
    1 teaspoon minced fresh ginger = 2.71g
    ½ teaspoon dried chile flakes, optional = 1.20g
    1 tablespoon fermented black beans, optional
    2 ½ to 3 tablespoons chile bean sauce, Pixian kind preferred = 70.79g
    1 generous teaspoon sugar
    2 teaspoons regular soy sauce
    1.3 c water = 290ml - 295ml
    Salt
    2 large green onions, white and green parts, cut on the diagonal into pieces about 1 ½ inches long
    1 ½ tablespoons cornstarch = 11.96g dissolved in 3 tablespoons water

Instructions

    Cut the tofu into ½-inch cubes and put into a bowl. Bring a kettle of water to a rolling boil. Turn off the heat and when the boiling subsides, pour water over the tofu to cover. Set aside for 15 minutes.
    Meanwhile, measure out 1 ⅓ cups of water = 290ml (the stuff you just boiled is fine) and set aside near the stove. You’ll be using it later for the sauce.
    In a large wok or skillet, toast the peppercorn over medium heat for 2 to 3 minutes, until richly fragrant and slightly darkened; you may see a wisp of smoke. Let it cool briefly, then pound with a mortar and pestle or grind in a spice grinder. Set aside.
    Drain the tofu in a strainer or colander and put it near the stove. As with all stir-fries, assemble your ingredients next to the stove.
    Mince ginger, add chile flake and dou ban jiang     
Heat the oil in the wok or skillet over high heat. Add the beef, stirring and mashing into small pieces until crumbly and cooked through, about 2 minutes. Add the ginger, chile flakes, fermented black beans, and chile sauce. Cook for about 2 minutes, stirring constantly, until the beef is a rich reddish-brown color and the chile sauce has turned the oil slightly red. Add the sugar and soy sauce, stir to combine, then add the tofu. Gently stir or give the wok a shake to combine without breaking up the tofu much.
    Pour in the 1 ⅓ cups water you set aside earlier. Bring to a vigorous simmer, and cook for about 3 minutes to allow the tofu to absorb the flavors of the sauce.
    Taste the sauce and add a pinch of salt or sugar, if needed. Add the green onion and stir to combine. Give the cornstarch one last stir, then pour enough into the wok to thicken the sauce. You may not need to use it all. In Sichuan, the sauce is more soupy than gravylike. Sprinkle in the ground peppercorn, give the mixture one last stir to incorporate, then transfer to a shallow bowl. Serve immediately with lots of hot rice.

Notes
Source: Andrea Nguyen's Asian Tofu (Ten Speed Press, 2012)
